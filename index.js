class Digit {
  constructor(element) {
    this.element = element
    this.lines = this.element.querySelectorAll(`.clock__line`)
  }

	show = (digit) => {
		switch (Number(digit)) {
			case 0:
				this.show0()
        break;
			case 1:
				this.show1()
        break;
			case 2:
				this.show2()
        break;
			case 3:
				this.show3()
        break;
			case 4:
				this.show4()
        break;
			case 5:
				this.show5()
        break;
			case 6:
				this.show6()
        break;
			case 7:
				this.show7()
        break;
			case 8:
				this.show8()
        break;
			case 9:
				this.show9()
        break;
		}
	}

	showLine = (line) => {
		line.classList.remove(`clock__line_hidden`)
	}
	hideLine = (line) => {
		line.classList.add(`clock__line_hidden`)
	}

	show0 = () => {
		this.showLine(this.lines[0])
		this.showLine(this.lines[1])
		this.showLine(this.lines[2])
		this.hideLine(this.lines[3])
		this.showLine(this.lines[4])
		this.showLine(this.lines[5])
		this.showLine(this.lines[6])
	}
	show1 = () => {
		this.hideLine(this.lines[0])
		this.hideLine(this.lines[1])
		this.showLine(this.lines[2])
		this.hideLine(this.lines[3])
		this.hideLine(this.lines[4])
		this.showLine(this.lines[5])
		this.hideLine(this.lines[6])
	}
	show2 = () => {
		this.showLine(this.lines[0])
		this.hideLine(this.lines[1])
		this.showLine(this.lines[2])
		this.showLine(this.lines[3])
		this.showLine(this.lines[4])
		this.hideLine(this.lines[5])
		this.showLine(this.lines[6])
	}
	show3 = () => {
		this.showLine(this.lines[0])
		this.hideLine(this.lines[1])
		this.showLine(this.lines[2])
		this.showLine(this.lines[3])
		this.hideLine(this.lines[4])
		this.showLine(this.lines[5])
		this.showLine(this.lines[6])
	}
	show4 = () => {
		this.hideLine(this.lines[0])
		this.showLine(this.lines[1])
		this.showLine(this.lines[2])
		this.showLine(this.lines[3])
		this.hideLine(this.lines[4])
		this.showLine(this.lines[5])
		this.hideLine(this.lines[6])
	}
	show5 = () => {
		this.showLine(this.lines[0])
		this.showLine(this.lines[1])
		this.hideLine(this.lines[2])
		this.showLine(this.lines[3])
		this.hideLine(this.lines[4])
		this.showLine(this.lines[5])
		this.showLine(this.lines[6])
	}
	show6 = () => {
		this.showLine(this.lines[0])
		this.showLine(this.lines[1])
		this.hideLine(this.lines[2])
		this.showLine(this.lines[3])
		this.showLine(this.lines[4])
		this.showLine(this.lines[5])
		this.showLine(this.lines[6])
	}
	show7 = () => {
		this.showLine(this.lines[0])
		this.hideLine(this.lines[1])
		this.showLine(this.lines[2])
		this.hideLine(this.lines[3])
		this.hideLine(this.lines[4])
		this.showLine(this.lines[5])
		this.hideLine(this.lines[6])
	}
	show8 = () => {
		this.showLine(this.lines[0])
		this.showLine(this.lines[1])
		this.showLine(this.lines[2])
		this.showLine(this.lines[3])
		this.showLine(this.lines[4])
		this.showLine(this.lines[5])
		this.showLine(this.lines[6])
	}
	show9 = () => {
		this.showLine(this.lines[0])
		this.showLine(this.lines[1])
		this.showLine(this.lines[2])
		this.showLine(this.lines[3])
		this.hideLine(this.lines[4])
		this.showLine(this.lines[5])
		this.showLine(this.lines[6])
	}
}

class Clock {
  constructor(id) {
    const element = document.getElementById(id)
    if (!element) {
    	throw new Error("Can't find an element with provided ID")
    }
    this.element = element
    this.digits = this.getDigits()

    this.startReal()
  }

  getDigits = () => {
  	const temp = this.element.querySelectorAll(`.clock__digit`)
  	const digits = []
  	temp.forEach(item => {
  		digits.push(new Digit(item))
  	})
  	return digits
  }

  startReal = () => {
		setInterval(() => {
			const hours = Math.floor(Math.random() * 24);
			const minutes = Math.floor(Math.random() * 60);
			console.log(hours, minutes)
			this.display(hours.toString(), minutes.toString())
		}, 4000)
  }

	display = (hours, minutes) => {
		if (hours[1] === undefined) {
			this.digits[0].show(0)
			this.digits[1].show(Number(hours[0]))
		} else {
			this.digits[0].show(Number(hours[0]))
			this.digits[1].show(Number(hours[1]))
		}
		if (minutes[1] === undefined) {
			this.digits[2].show(0)
			this.digits[3].show(Number(minutes[0]))
		} else {
			this.digits[2].show(Number(minutes[0]))
			this.digits[3].show(Number(minutes[1]))
		}
	}
}

const clock = new Clock("clock");